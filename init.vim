syntax enable
filetype plugin indent on

let g:mapleader="\<Space>"
let g:lf_map_keys = 0
set clipboard+=unnamedplus
set autoread
set ruler
set mat=2
set scrolloff=8
set magic
set hlsearch
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set cursorline
set ffs=unix,dos,mac
set wrap
set pumheight=10
set nobackup
set nowritebackup
set noswapfile
set completeopt=menuone,noselect,noinsert
set shortmess+=c
set title
set wildmenu
set wildignore+=**/.git/**
set formatoptions-=cro
set ignorecase
set smartcase
set incsearch
set termguicolors
set number
set list
set listchars=
set listchars+=tab:›\ ,
set listchars+=trail:•,
set fillchars+=vert:\ ,
set mouse+=a

au TextYankPost * silent! lua vim.highlight.on_yank{ timeout = 250 }
au VimResized * wincmd =
au BufWritePre * %s/\s\+$//e
au StdinReadPre * let s:std_in=1
au BufReadPost *.js *.css *.html :set sw=2 ts=2

lua << EOF
vim.cmd("cd %:p:h")
require("plugins")
require("candy")
EOF

lua << EOF
require'bufferline'.setup{
    options = {
        view = "multiwindow",
        numbers = "none",
        buffer_close_icon= '',
        modified_icon = '',
        close_icon = '',
        left_trunc_marker = '',
        right_trunc_marker = '',
        max_name_length = 8,
        max_prefix_length = 1, -- prefix used when a buffer is deduplicated
        tab_size = 12,
        show_buffer_close_icons = true,
        separator_style = "thin", -- thin, thick, slant
        always_show_bufferline = true,
    }
}
EOF

map <leader>f :Files <CR>
map <leader>e :> <CR>
map <leader>q :< <CR>
map <leader>p :Snippets <CR>
map <Tab> :noh<CR>
map <leader>s :TagbarToggle<CR>
map <leader><Space> :Lf<CR>
map <leader>t :FloatermToggle<CR>

nnoremap <silent><C-l> :BufferLineCycleNext<CR>
nnoremap <silent><C-h> :BufferLineCyclePrev<CR>
nnoremap <silent><C-s> :BufferLinePick<CR>
nnoremap <silent><C-q> :Bclose<CR>

noremap <C-n> :NvimTreeToggle<CR>

nnoremap <C-A-j> :m .+1<CR>==
nnoremap <C-A-k> :m .-2<CR>==

noremap <silent> <Left> :vertical resize +3<CR>
noremap <silent> <Right> :vertical resize -3<CR>
noremap <silent> <Up> :resize +3<CR>
noremap <silent> <Down> :resize -3<CR>

cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!
