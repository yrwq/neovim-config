> This setup requires the most current version of neovim!

## Setup

1. Install [neovim](https://github.com/neovim/neovim)

* Download official neovim appimage

```bash
curl -L https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -o nvim
```

* Make nvim executable

```bash
chmod +x nvim
```

* Copy/move nvim to bin

```bash
sudo mv nvim /usr/bin/
```

2. Install [packer.nvim](https://github.com/wbthomason/packer.nvim)

```bash
git clone https://github.com/wbthomason/packer.nvim \
 ~/.local/share/nvim/site/pack/packer/opt/packer.nvim
```

3. Clone this repository to `.config/nvim`

> Back up your current configuration now, if you haven't already!

```bash
git clone https://github.com/yrwq/neovim-config ~/.config/nvim
```

> Now you can launch nvim, if you expect any errors, ignore them and restart neovim.
