-- helper for defining highlight groups
local set_hl = function(group, options)
    local bg = options.bg == nil and '' or 'guibg=' .. options.bg
    local fg = options.fg == nil and '' or 'guifg=' .. options.fg
    local gui = options.gui == nil and '' or 'gui=' .. options.gui
    local link = options.link or false
    local target = options.target

    if not link then
      vim.cmd(string.format(
        'hi %s %s %s %s',
        group, bg, fg, gui
      ))
    else
      vim.cmd(string.format(
        'hi! link', group, target
      ))
    end
end

override_colors = function()
  local highlights = {
    -- normal stuff
    {'Normal', { bg = 'NONE' }},
    {'Comment', { gui = 'italic' }},
    {'SignColumn', { bg = 'NONE' }},
    {'ColorColumn', { bg = '#322b28' }},
    {'EndOfBuffer', { bg = 'NONE', fg = '#322b28' }},
    {'IncSearch', { bg = '#322b28' }},
    {'String', { gui = 'NONE' }},
    {'Special', { gui = 'NONE' }},

    -- tabline stuff
    {'Tabline', { bg = 'NONE' }},
    {'TablineSuccess', { bg = '#322b28', fg = '#947e68', gui = 'bold' }},
    {'TablineError', { bg='NONE', fg='#6d413b' }},
    {'TablineWarning', { bg='NONE', fg='#7e5d3c' }},
    {'TablineInformation', { bg='NONE', fg='#527272' }},
    {'TablineHint', { bg='NONE', fg='#6a7253' }},

    -- lsp saga stuff
    -- {'TargetWord', { fg = '#6d413b', bg = 'NONE', gui = 'bold' }},

    -- misc
    {'IncSearch', { bg='#322b28', fg='#947e68' }},

    -- lspsaga
    {'LspFloatWinBorder',             {bg = 'NONE', fg = '#947e68'}},
    {'ProviderTruncateLine',          {bg = 'NONE', fg = '#947e68'}},
    {'LspSagaDocTruncateLine',        {bg = 'NONE', fg = '#947e68'}},
    {'LspSagaCodeActionTruncateLine', {bg = 'NONE', fg = '#947e68'}},

    -- nvimtree
    {'NvimTreeFolderIcon', { fg = '#947e68' }},
    {'NvimTreeIndentMarker', { fg = '#947e68' }},

  }

  for _, highlight in pairs(highlights) do
    set_hl(highlight[1], highlight[2])
  end
end

-- italicize comments
set_hl('Comment', { gui = 'italic' })

-- automatically override colourscheme
vim.api.nvim_exec([[
    augroup NewColor
    au!
    au ColorScheme tlou2 call v:lua.override_colors()
    augroup END
]], false)

-- disable invert selection for gruvbox
vim.g.gruvbox_invert_selection = false
vim.cmd('colorscheme tlou2')
