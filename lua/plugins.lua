local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
    execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
    execute('packadd packer.nvim')
end

vim.cmd 'packadd cfilter'
vim.cmd 'packadd packer.nvim'

local init = function ()
    use {'wbthomason/packer.nvim', opt = true}

    use "tpope/vim-surround"            -- '" :()
    use "hoob3rt/lualine.nvim"          -- the status bar
    use "luochen1990/rainbow"           -- higlight parentheses
    use "tpope/vim-commentary"          -- commenting made easier
    use "psliwka/vim-smoothie"          -- smoth scrolling
    use "sheerun/vim-polyglot"          -- better syntax higlighting
    use "rbgrouleff/bclose.vim"         -- highlight when closing parentheses
    use "kyazdani42/nvim-tree.lua"      -- file tree viewer
    use "lifepillar/vim-gruvbox8"       -- gruvbox 🤎
    use "ptzz/lf.vim"                   -- use lf file manager in vim
    use "voldikss/vim-floaterm"         -- terminal inside vim
    use "windwp/nvim-autopairs"         -- highlight parentheses
    use "norcalli/nvim-colorizer.lua"   -- highlight colors
    use "akinsho/nvim-bufferline.lua"   -- bufferline

    use {
        "glepnir/galaxyline.nvim",
        branch = "main",
        config = function() require"my_statusline" end,
    }

    -- fuzzy
    use {
        "junegunn/fzf.vim",
        requires = "junegunn/fzf"
    }

    -- snippets
    use {
        "SirVer/ultisnips",
        requires = "honza/vim-snippets"
    }

    -- fancy icons
    use {
        "ryanoasis/vim-devicons",
        "kyazdani42/nvim-web-devicons",
    }

end


return require('packer').startup(init)

